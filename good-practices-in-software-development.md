### Which point(s) were new to you?

- Some companies use tools like Trello, Jira for documentation. But if your team is not using any such tool, you should write down the requirements and share it with the team. This will help you get immediate feedback. Also, this will serve as a reference for future conversations
- Tracking your time using app like Boosted to improve productivity.
- The way you ask a question determines whether it will be answered or not. You need to make it very easy for the person to answer your question. Messages like the database is not connecting, need your help or the build is not working, can you help me out, will not get you the answers you are looking for.

### Which area do you think you need to improve on? What are your ideas to make progress in that area?

- Communication is key in transferring information.
- I think I need to increase my communication while talking with higher positioned employes. 
- To improve in this case, I need to maintain rules like talking casual when required and maintaing both professional and casual.

