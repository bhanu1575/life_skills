# Sexual Harassment
Sexual harassment is any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment. It can be categorized into verbal, visual, and physical forms, and can lead to quid pro quo or hostile work environment situations. Employers can be held liable if they knew or should have known about the harassment and failed to take appropriate corrective action.

## What kinds of behaviour cause sexual harassment?
sexual harassment is about the impact on the recipient, not the intent of the person behaving inappropriately. Even if someone doesn't mean to cause offense, their behavior can still be considered sexual harassment if it makes the other person feel uncomfortable, unsafe, or humiliated.

### [Forms of Sexual Harassment]( https://www.youtube.com/watch?v=Ue3BTGW3uRQ)

- **Verbal harassment** can include a wide range of behaviors, such as making offensive comments or jokes, requesting sexual favors, or spreading rumors. These actions contribute to a hostile work environment.

- **Visual harassment** through explicit images, cartoons, or inappropriate digital content can create an offensive workplace and negatively impact employees’ well-being and productivity.

- **Physical harassment**, including inappropriate touching or sexual gestures, can lead to a hostile work environment and even physical assault. These actions are severe forms of sexual harassment.

### If I witness harassment or bullying:

- **Interfere if it is safe**: Depending on the severity of the situation, I might try to de-escalate by calmly approaching the people involved and separating them.
- **Report the incident**: This could involve finding a person in authority, like a manager, teacher, or security guard. There might also be anonymous reporting channels available.
- **Offer support to the victim**: Let them know you saw what happened and that you're there for them.

### If I am the target of harassment or bullying:

- **Document the incidents**: Keep a record of the dates, times, and details of what happened. This could be helpful if you need to report the behavior officially.
- **Set boundaries**: Clearly and firmly tell the person to stop their harassing behavior.
- **Report the incident**: Follow the same steps as mentioned for witnessing an incident.


