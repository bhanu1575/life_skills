### Introduction to Grit.
Grit, defined as passion and perseverance for long-term goals, is a significant predictor of success in many ways. However, there is still limited knowledge on how to build grit in individuals, especially in children. One of the best approach to build grit is promoting a growth mindset.
### Fixed Mindset vs Growth Mindset
- Fixed Mindset: 
    - believe that skills  and intelligence are set  and you either have them or you don't,
    - that some people are just naturally  good at things, while others are not.
    - believe  that you are not in control  of your abilities.
- Growth Mindset:
    - believe that skills  and intelligence are grown and developed.
    - people who are good at something  are good because they built that ability, and people who aren't are not good because they haven't done the work.
    - believe  that you are in control of your abilities.
### What is the Internal Locus of Control?
- Internal Focus of control: 
    - Belief that your effort and actions determine your outcomes.
    - Focus on the factors which are under your control.
    - Recognizing how your actions solve problems in your life.
- key points
    - The study found that students who were praised for their effort and hard work instead of their intelligence showed higher levels of motivation. 
    - Students who were praised for their intelligence showed lower motivation and believed that external factors determined their success. 
    - Taking credit for the positive outcomes you achieve.
    - Developing an internal locus of control is key to staying motivated.
### How to build a Growth Mindset?
- believe in your ability to figure things out
- question your assumptions
- develop your own life curriculum
- Honoring the struggle
### What are your ideas to take action and build Growth Mindset?
- Take responsibility for your actions.
- putting efforts is the initial step for a Growth Mindset.
- embrace challenges, see mistakes as learning opportunities, and appreciate feedback.