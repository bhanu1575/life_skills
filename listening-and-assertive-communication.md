## What are the steps/strategies to do Active Listening?

- Avoid getting distracted by your own thoughts, focus on the speaker and topic
- Try not to interrupt, let the other person finish and then respond
- Use door openers to show interest and keep the conversation flowing
- Show listening through appropriate body language and take notes during important conversations
- Paraphrase what others have said to ensure understanding
- Acknowledge their emotions

## According to Fisher's model, what are the key points of Reflective Listening?

- focus on understanding the speaker's point of view, rather than their own.
- listen with empathy, acceptance.
- respond to the feelings of the speaker, not just the content of their words.
- use reflective statements to restate what the speaker said.

## What are the obstacles in your listening process?

- **Distractions:** Internal toughts always pulls my attention away from the speaker.
- **Boredom:** I lose focus or curiosity, If the topic seems uninteresting.
- **Emotional State:** Feeling anxious, angry, or upset can make it hard to concentrate on what other person is saying.

## What can you do to improve your listening?

- **Pay Attention:** Give the speaker your full attention. Put away distractions like your phone and make eye contact.
- **Be Present:** Focus on the here and now of the conversation. Don't get caught up in formulating your response while the other person is still talking.
- **Show Interest:** Use nonverbal cues like nodding and leaning in to show you're engaged.
- **Listen with Empathy:** Try to understand the speaker's perspective and feelings, even if you don't agree with them.

## When do you switch to Passive communication style in your day to day life?

passive communication might be used in situations like:
- Avoiding Conflict.
- Protecting Myself.
- When I Don't Have Strong Opinions.

## When do you switch into Aggressive communication styles in your day to day life?

- when a person is threatened.
- to show the dominance/ authority over other person.
- when dealing with Manipulation or Abuse.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

-  person feels Unheard/ Unvalued.
-  person lacks accountability.
-  person has unhealthy relationships.

## How can you make your communication assertive? what would be a few steps you can apply in your own life?

- Active listening.
- Talking with confident tone.
- Being self aware
- Maintaining good Eye contact, Body language, and Facial Expressions.







