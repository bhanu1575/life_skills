# What is the Feynman Technique?
The Feynman Technique is a learning method where you explain a concept as if you're teaching it to someone who has no prior knowledge.

#  what was the most interesting story or idea for you?
- The speaker struggled with math and science in school but became an engineering professor later in life.
- She credits her success to learning effective learning techniques.

# What are active and diffused modes of thinking?

- **Active or Focused Mode:** This is when you consciously concentrate your attention on something specific. It's ideal for tasks you're familiar with, like solving problems using established knowledge or memorizing facts.

- **Relaxed or Diffused Mode:** This is a more relaxed state where your brain makes unconscious connections and processes information passively. It's helpful for coming up with new ideas, seeing problems from fresh perspectives, and allowing the "aha!" moments to happen.


# what are the steps to take when approaching a new topic? 

- **Deconstruct the skill:** Break down the skill into smaller, more manageable pieces.
- **Learn enough to self-correct:** Gather resources to learn the basics and be able to identify your mistakes while practicing.
- **Remove barriers to practice:** Minimize distractions that might hinder your ability to focus on practicing.
- **Practice for at least 20 hours:** Commit to practicing for a set amount of time to overcome the initial difficulty and see improvement.


# What are some of the actions you can take going forward to improve your learning process?

- Set a 20-hour commitment
- Break down the skill
- Focus on deliberate practice
- Find resources for self-correction
- Minimize distractions
- Embrace the initial difficulty

# References

- [How to Learn Faster with the Feynman Technique](https://www.youtube.com/watch?v=_f-qkGJBPts)
- [Learning how to learn | Barbara Oakley | TEDxOaklandUniversity](https://www.youtube.com/watch?v=O96fE1E-rf8)
- [The first 20 hours -- how to learn anything | Josh Kaufman | TEDxCSU](https://www.youtube.com/watch?v=5MgBikgcWnY)