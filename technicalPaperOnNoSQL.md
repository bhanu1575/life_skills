# NoSQL
NoSQL refers to a type of database management system (DBMS) designed to handle large amounts of unstructured or semi-structured data. Unlike relational databases that store data in rigid tables, NoSQL databases offer more flexibility in how data is stored and accessed.

## When should NoSQL be used?
When deciding which database to use, decision-makers typically find one or more of the following factors lead them to selecting a NoSQL database:
- Fast-paced Agile development
+ Storage of structured and semi-structured data
- Huge volumes of data
+ Requirements for scale-out architecture
- Modern application paradigms like microservices and real-time streaming

NoSQL is mainly used in Big Data Applications, Real-time Web Applications, Unstructured Data Storage

Here are the top 5 widely used NoSQL databases.

## MongoDB
mongoDB comes under the category of document-oriented databases.

- Data Model: 
    - Stores data in flexible JSON-like documents. These documents can have various structures and don't need to conform to a rigid schema like relational database tables. This makes it easier to store complex data with varying fields.

- Scalability:
    - Horizontally scalable. You can add more servers to your MongoDB cluster to handle increasing data volumes and user traffic. This makes it a good choice for applications with growing data needs.

- Performance:
    - MongoDB offers good performance for both reads and writes. Document-oriented storage allows for efficient retrieval of data that is often accessed together.

- Limitations:
    - Data Consistency: By default, MongoDB prioritizes performance over strict data consistency. This may not be ideal for applications requiring strong data integrity guarantees.
    - Complex Joins: Due to its document-oriented nature, complex joins across large datasets might not be as efficient as in relational databases.

- It is mostly used in Web Applications, E-commerce, Real-time Analytics, Content Management Systems.


##  Redis
Redis is a powerful open-source NoSQL database that stands out for its speed and versatility. Unlike MongoDB which focuses on document-oriented storage, Redis is a key-value store.

- Data Model: 
    - Key-value store: Data is stored as key-value pairs. Keys are unique identifiers used to retrieve the corresponding value. Values can be of various data types like strings, lists, sets, sorted sets, and more. This structure allows for very fast data access and manipulation.

- Performance: 
    - Redis excels in speed. Because it stores data entirely in memory (RAM), read and write operations are blazing fast. This makes it ideal for caching frequently accessed data, real-time applications, and leaderboards.

- Limitations: 
    - Data Volatility: In-memory storage means data is lost upon server restarts unless persistence is configured.
    - Limited Query Capabilities: Redis is not designed for complex queries like relational databases. It excels at simple key-value lookups and operations on specific data structures.

- It is mostly used in Caching, Session Management, real-time messaging, leaderboards, etc.

## Apache Cassandra
It is a free, open-source, distributed NoSQL database designed for handling massive amounts of data across multiple commodity servers. It prioritizes high availability, scalability, and fault tolerance, making it a strong choice for large-scale applications that require continuous uptime and the ability to handle ever-increasing data demands.

- Data Model: 
    - Cassandra uses a wide-column store model. Data is organized into tables with rows and columns, but unlike relational databases, columns can have a dynamic structure. This flexibility allows for storing various data types within a row.

- Scalability:
    - Cassandra excels at horizontal scaling. You can easily add more servers to your cluster to handle growing data volumes and user traffic. This linear scalability makes it suitable for applications that anticipate significant data growth.

- Limitations:
    - Complexity: Managing a distributed database cluster can be more complex compared to simpler NoSQL databases.
    - Data Consistency: Tunable consistency requires careful consideration to balance availability and data integrity needs for your application. Eventual consistency might not be suitable for all scenarios.
    - Joins: Complex joins across large datasets might not be as efficient in Cassandra compared to relational databases.

- It is widely used in Distributed databases, Telecommunications

## Amazon DynamoDB 
it is a highly scalable and fault-tolerant NoSQL database service offered by Amazon Web Services (AWS). It's a serverless option that automatically provisions and manages resources, allowing you to focus on your application logic without worrying about database administration tasks.

- Data Model:
    - Key-Value Store: DynamoDB stores data as key-value pairs. You define a primary key that uniquely identifies each item (data record) in your table. Additionally, you can optionally define a sort key for further data organization within a partition.
    - Flexible Schema: DynamoDB is schema-less, meaning you don't need to define a rigid structure for your data upfront. This allows for flexibility in storing data with varying attributes.

- Scalability :
    - DynamoDB excels at horizontal scaling. You don't need to pre-provision capacity; instead, DynamoDB automatically scales storage and throughput based on your application's needs. This makes it ideal for applications with unpredictable traffic patterns or bursty workloads.

- Limitations: 
    - Limited Query Capabilities: While DynamoDB offers basic querying capabilities, it might not be ideal for complex queries that require joins across large datasets. For such scenarios, consider using DynamoDB in conjunction with other AWS services like Amazon RDS (relational database service).
    - Eventual Consistency: By default, DynamoDB offers eventual consistency, meaning writes might not be immediately reflected across all replicas. This may not be suitable for applications requiring strong consistency guarantees for every read. You can configure DynamoDB for stronger consistency at the expense of some performance.

- It is mostly used in Mobile Backends, Internet of Things (IoT), etc.


## Couchbase
It is a versatile NoSQL database that stands out for its ability to handle multiple data models and offer SQL querying capabilities alongside NoSQL access methods.

- Data Model:
    - Unlike many NoSQL databases that focus on a single data model (e.g., documents, key-value pairs), Couchbase offers flexibility by supporting several models:
        - Document Model: Store data in JSON-like documents with flexible schemas, ideal for complex data structures.
        - Key-Value Model: Access data efficiently using unique keys, suitable for simple lookups and frequently accessed data.
        - SQL Querying: Even though it's a NoSQL database, Couchbase allows querying data using a SQL-like syntax called N1QL. This can simplify data access for developers familiar with SQL.

- Scalability: 
    - Horizontal scaling is achieved by adding more servers to your Couchbase cluster, allowing you to handle increasing data volumes and user traffic.

- Performance:
    - Couchbase is built for performance and scalability. It leverages an in-memory data caching layer combined with persistent storage on disk. This architecture enables fast reads and writes while ensuring data durability.

- Limitations:
    - Complexity: Compared to simpler NoSQL databases, managing a Couchbase cluster might involve a slight learning curve due to its multi-model nature.
    - Cost: Depending on your deployment choice (on-premises or cloud) and required features, Couchbase licensing might have different cost considerations compared to some open-source NoSQL options.

- It is mostly used in applications requiring flexibility, data richness, and real-time data access.

## References
- https://www.knowledgenile.com/blogs/pros-and-cons-of-mongodb#:~:text=Scalability%20is%20one%20of%20the,MongoDB%20to%20use%20horizontal%20scalability.
- https://certisured.com/blogs/the-redis-realm-exploring-the-world-of-high-performance-data-storage
- https://medium.com/@VIGNESH-MURUGESAN/apache-cassandra-d0ceece8926c
- https://www.allthingsdistributed.com/2012/01/amazon-dynamodb.html#:~:text=There%20are%20no%20pre%2Ddefined,throughput%20at%20very%20low%20latency.
- https://medium.com/@abhijeetkb/couchbase-an-introduction-to-a-scalable-nosql-database-58ebf7ca2c55
