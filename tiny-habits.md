### what was the most interesting story or idea for you?
- Celebrating victories.
- Starting with tiny habits is more effective than aiming for big changes. 
- By breaking down big goals into small actions, the chances of success increase significantly.
- Format for Tiny Habit: 
    - After I (existing habit), I will (new tiny habit)

### How can you use B = MAP to make making new habits easier? What are M, A and P.
- Shrink habits to the tiniest version to reduce the need for motivation
- Use action prompts that leverage momentum from previous behaviors
- Celebrate tiny wins to create success momentum and increase motivation
- M stands for Motivation.
- A stands for Ability.
- P stands for prompt.
### Why it is important to "Shine" or Celebrate after each successful completion of habit?
- **Shine is like a reward:** It's the feeling similar to getting a reward, which makes the habit loop and you want to repeat the behavior.
- **Success momentum:** Celebrating small wins builds confidence and motivation. The more you celebrate these tiny successes, the more you'll want to repeat the habit. This creates a cycle of success that helps the habit grow.
- **Motivation grows, not from size, but from frequency:** Celebrating frequently, even for small wins, is more effective than waiting for a big win to celebrate. This keeps you motivated.

### what was the most interesting story or idea for you from 1% Better Every Day Video?
- Small improvements and choices compound over time and can be the key to unlocking significant success in any area.
- Four stages of habit formation:
    - noticing
    - wanting
    - doing
    - liking
- implementation intentions, or specific plans, can significantly increase the chances of success in forming new habits.
- Failure pre-mortem and if-then plans can help overcome challenges and ensure better planning for habit formation.
### What is the book's perspective about Identity?
people set goals based on desired outcomes e.g., lose weight. But these goals are temporary and don't provide long-term motivation. The book suggests focusing on identity-based goals.
Example: Instead of aiming to "lose weight," aim to "become someone who exercises daily." This identity shift makes the habit more sustainable because you're working towards who you want to be.
### Write about the book's perspective on how to make a habit easier to do?
- Focus on Systems over Goals
- Make it Obvious
- Make it Attractive
- Make it Easy/Simple/Tiny
- Give yourself an immediate reward
### Write about the book's perspective on how to make a habit harder to do?
- Reducing the triggers that prompt the habit.
- Having low desire towards the habit.
- Increase the steps between you and the habit.
- Not rewarding and celebrating the outcome of an habit.
### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I would like to develop the habit of solving at least one coding problem a daily.
- To make it more Obvious (Cue): set a particular time period
- Make it Attractive (Craving): look into others solution and try to make it better than the previous solution.
- Make it Easy (Response): Start with solving easy problems
- Make it Satisfying (Reward): Celebrate each time you solve a problem.
### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I would like to eliminate the habit of playing mobile games too much.
- Make the Cue Invisible (Reduce Triggers): Keeping my phone away from me.
- Make the Process Unattractive (Reduce Craving): Pairing playing games with a slightly unpleasant task. Do 10 push-ups every time you open a game app.
- Make it Difficult: increase the friction between me and my habit like having no data in my phone.
- Make it Unsatisfying (Reduce Reward): finding other tasks which are more rewarding.
