### What are the activities you do that make you relax - Calm quadrant?

- Serenity
- Expectation
- Sleepiness
- Relaxation

### When do you find getting into the Stress quadrant?

- Anger 
- Tension
- Disgust
- Fear

### How do you understand if you are in the Excitement quadrant?

You are in the Excitement quadrant when you feel
- Surprise
- Satisfaction
- Enjoyment
- Happiness

### Sleep is your superpower

- Sleep is vital for learning and memory, before and after studying. 
- It also impacts reproductive health and increases disease risk. 
- Deep sleep aids memory consolidation. 
- Poor sleep links to aging and diseases like dementia. 
- Brain stimulation during sleep can boost memory. 
- Sleep loss weakens the immune system and raises cancer risk. 
- Tips for better sleep include consistency and a cool bedroom.

### What are some ideas that you can implement to sleep better?

- Maintain a consistent bedtime and wake-up time every day.
- Keep your bedroom cool, around 65°F (18°C), for optimal sleep.
- Avoid caffeine and alcohol close to bedtime.
- Limit daytime naps if you're having trouble sleeping at night.
- If you're unable to sleep after a while, get out of bed and do a relaxing activity until you feel sleepy again.
- Consider using relaxation techniques such as deep breathing or meditation before bedtime.
- Create a comfortable sleep environment by investing in a good mattress and pillows.
- Establish a relaxing bedtime routine to signal to your body that it's time to wind down.
- Limit exposure to screens (phones, computers, TVs) before bedtime, as blue light can disrupt sleep.
- Seek professional help if you have persistent sleep problems or insomnia.

### Brain Changing Benefits of Exercise

- Exercise boosts brain chemicals immediately, making you feel good. 
- Regular exercise changes your brain's structure, improving memory and focus. 
- It also protects against age-related brain illnesses. 
- Aim for 3-4 sessions of 30 minutes weekly, focusing on activities that elevate your heart rate. 
- Consistent exercise promotes long-term happiness and brain health.

### What are some steps you can take to exercise more?Start small with short sessions of exercise.

- Set achievable goals and gradually increase intensity.
- Find enjoyable activities like dancing or hiking.
- Schedule regular workouts and stick to them.
- Mix up your routine with cardio, strength, and flexibility exercises.